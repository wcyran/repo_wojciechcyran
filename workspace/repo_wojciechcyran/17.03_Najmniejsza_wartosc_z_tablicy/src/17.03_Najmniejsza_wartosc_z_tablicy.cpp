  // Program znajdujący najmniejszą z tablicy liczb


#include <iostream>
using namespace std;

int minimalna (int t[], int tab)
{
	int minimum = 100000;
	for (int i=0; i<tab; i++)
	{
		if (t[i]<minimum)
		{
			minimum=t[i];
		}
	}
		return (minimum);
}
int main()
{
	const int tab=6;
	int t[tab]={1,2,3,4,5,6};

	cout << "Program obliczajacy wartosc minimalna " <<'\n';

	int minimum = minimalna (t, tab);

	cout <<minimum;

	return 0;
}
