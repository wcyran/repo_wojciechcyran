// inserting into a vector
#include <iostream>
#include <vector>

int main ()
{
  std::vector<int> myvector (3,100);
  std::vector<int>::iterator it;

  it = myvector.begin();

  it = myvector.insert ( it , 200 );

  myvector.insert (it,1,20);

  // "it" no longer valid, get a new one:
  it = myvector.begin();

  std::vector<int> anothervector (1,9);
  myvector.insert (it+2,anothervector.begin(),anothervector.end());

  std::vector<int> anothervectorX (1,6);
  myvector.insert (it+2,anothervector.begin(),anothervector.end());

  //myvector.resize(5);
  myvector.resize(5,69);
  //myvector.resize(12);

//  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  // erase the 6th element
//    myvector.erase (myvector.begin()+3);
//
//    																// erase the first 3 elements:
//    myvector.erase (myvector.begin(),myvector.begin()+3);

  int myarray [] = { 501,502,503 };
  myvector.insert (myvector.begin(), myarray, myarray+3);


  std::cout << "myvector contains:";
  for (it=myvector.begin(); it<myvector.end(); it++)
    std::cout << ' ' << *it;
  std::cout << '\n';

  int sum =0;
  while (!myvector.empty())
    {
       sum += myvector.back();
       myvector.pop_back();
    }
  std::cout << "total: " << sum << '\n';
  std::cout << "size: " << myvector.sum << '\n';

  myvector.clear();
  std::cout << "size: " << (int) myvector.size() << '\n';
   std::cout << "capacity: " << (int) myvector.capacity() << '\n';
   std::cout << "max_size: " << (int) myvector.max_size() << '\n';

  return 0;
}
