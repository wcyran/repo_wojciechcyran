#include <iostream>

using namespace std;

const int tab=6;
int t[6]={2,3,40,3,2,1};

int x;

int liczb_pow=0;

int main()
{
    cout << "Program zliczajacy powtorzenia w tablicy" << endl;

    for (int i=0; i<6; i++){
    	for (int j=i+1; j<6; j++){ //porownujemy kolejne liczby z tablicy
    		if(t[i]==t[j])
    		liczb_pow++;    //zliczamy powtorzenia
    	}

    }
    cout << "Powtarzaja sie  "<<liczb_pow<<" liczby"<<endl;
    return 0;
}
