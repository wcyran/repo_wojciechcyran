//============================================================================
// Name        : Pola_figur.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

//Program obliczaj�cy pola powierzchni figur
//i ich obj�to�ci: sze�cian, prostopad�o�cian, kula, ostros�up, walec

#include <iostream>
#include <cmath>

using namespace std;


int wybor, a,b,c,H;

int main()
{
	cout << "Program obliczajacy pola figur" << endl;

		cout << "1. SZESCIAN" << endl;
		cout << "2. PROSTOPADLOSCIAN" << endl;
		cout << "3. KULA" << endl;
		cout << "4. OSTROSLUP PRAWIDLOWY" << endl;
		cout << "5. WALEC" << endl;
		cout << "0. KONIEC" << endl;

		cout << "Pole powierzchni jakiej bryly chcesz policzyc: " << endl;
		cin >> wybor;

	while (wybor!=0)
	{
		switch (wybor)
		{
			case 1:
				{
				cout << "Podaj dlugosc boku: " << endl;
				cin >> a;
				cout << "Pole powierzchni szescianu wynosi: " << 6 * a * a << endl;
				cout << "Objetosc szescianu wynosi: " << pow(a, 3)<<endl;
				break;
				}
			case 2:
				{
				cout << "Podaj dlugosc bokow a, b, c: " << endl;
				cin >> a >> b >> c;
				cout << "Pole powierzchni prostopadloscianu wynosi: "
						<< 2 * a * b + 2 * a * c + 2 * b * c << endl;
				cout << "Objetosc prostopadloscianu wynosi: " << a * b * c<<endl;
				break;
				}
			case 3:
				{
				cout << "Podaj promien kuli: " << endl;
				cin >> a;
				cout << "Pole powierzchni kuli wynosi: " << 4 * M_PI * (pow(a, 2));
				cout << "Obj�to�c kuli wynosi: " << (4.0 / 3.0) * M_PI * pow(a, 3)<<endl;
				break;
				}
			case 4:
				{
				cout << "Podaj dlugosc krawedzi podstawy: " << endl;
				cin >> a;
				cout << "Podaj liczbe krawedzi bocznych oraz dlugosc sciany bocznej"
						<< endl;
				cin >> b >> c;
				cout << "Podaj wysokosc ostroslupa: " << endl;
				cin >> H;
				cout << "Pole powierzchni ostroslupu wynosi: "
						<< ((a * a) + (b / 2.0) * a * c) << endl;
				cout << "Obj�tosc ostroslupa wynosi: " << (1.0 / 3.0) * a * a * H<<endl;
				break;
				}
			case 5:
				{
				cout << "Podaj promien i wysokosc walca" << endl;
				cin >> a >> b;
				cout << "Pole walca wynosi: " << 2 * M_PI * a * b + M_PI * a * a
						<< endl;
				cout << "Obj�tosc walca wynosi: " << M_PI * pow(a, 2) * b<< endl;
				break;
				}
		}
	}
	cout<<"KONIEC";

	return 0;
}
