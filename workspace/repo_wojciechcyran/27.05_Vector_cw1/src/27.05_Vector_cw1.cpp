// vector::push_back
#include <iostream>
#include <vector>

int main ()
{
  std::vector<int> myvector;
  int myint;

  std::cout << "Please enter some integers (enter 0 to end):\n";

  do
  {
    std::cin >> myint;
    myvector.push_back (myint);
  }
  while (myint);

  std::cout << "myvector stores " << int(myvector.size()) << " numbers.\n";

//  vector<int>::iterator it = Myvector.begin();
//
//  for (int i=0; i<Myvector.size();++i)
//  {
//	  std::cout << ' ' << *it;
//	  std::cout << '\n';
//  }
  return 0;
}
