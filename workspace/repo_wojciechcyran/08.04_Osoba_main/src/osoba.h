/*
 * osoba.h
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include <iostream>
#include <string>

#ifndef OSOBA_H_
#define OSOBA_H_

// class b

class osoba {
public:
	osoba();
	osoba (std::string imie, std::string nazwisko, int wiek);
	void opis(std::string imie, std::string nazwisko, int wiek);
	void wypisz();
	virtual ~osoba();
//private:
	std::string imie;
	std::string nazwisko;
	int wiek;
// friend b; 		metoda pozwala korzystac na wzystko co zadeklarowalismy w .h
};

#endif /* OSOBA_H_ */
