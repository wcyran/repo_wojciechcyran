#ifndef OSOBA_H
#define OSOBA_H
#include <iostream>
#include <string>

class Osoba
{

    public:

    Osoba();
    Osoba(std::string status, std::string imie, std::string nazwisko, std::string email, int wiek);
    void opis ();

    protected:

    private:
    std::string nazwisko;
    std::string imie;
    int wiek;
    std::string email;
    std::string status;
};

#endif // OSOBA_H
