
#include <iostream>
#include <string>
#include "Room.h"
#include "Osoba.h"
#include "Kurs.h"

using namespace std;
/*void wypiszKursy(Kurs k1)
{
    tab_kursu[2]
    for (int i=0; i<3; i++)
    {
        std::cout"---Kursy do wyboru ---"<<tab_osob[i].opisKursu();
    }
}*/
int main()

{
    Room tab_Room [3];
    tab_Room [0]=Room(1,30,1);//Matematyka
    tab_Room [1]=Room(2,30,2);
    tab_Room [2]=Room(3,30,3);

    Osoba tab_osob [6];
	tab_osob[0]=Osoba("Student","Karol","Zdeb","zdeb@gmail.com", 24);
	tab_osob[1]=Osoba("Student", "Ela", "Fula","ela@gmail", 34);
	tab_osob[2]=Osoba("Student","Wojciech", "Cyran", "wcyran@gmail.com", 29);

	tab_osob[3]=Osoba("Profesor","Jerzy","Grebosz","jerzy@gmail.com", 65);
	tab_osob[4]=Osoba("Profesor","Zbigniew","Pieczonka","pieczony@gmail.com", 52);
	tab_osob[5]=Osoba("Profesor","Stanislaw","Czech","czech@gmail.com", 68);

	Kurs tab_kurs[3];

	tab_kurs [0]=Kurs("Matematyka","01.01.2010", "12.12.2015");
	tab_kurs [1]=Kurs("Fizyka","01.01.2010", "12.12.2015");
	tab_kurs [2]=Kurs("Geografia","01.01.2010", "12.12.2015");

	std::cout<<"-------------------"<<std::endl;

    Kurs k1;

	std::cout<<"-------------------"<<std::endl;

    k1.kursanci(&tab_osob[1]);
    k1.prowadzKurs(&tab_osob[3]);
    k1.dodajSale(&tab_Room[0]);

    k1.opis();


    return 0;
}
