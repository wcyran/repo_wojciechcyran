/*
 * Kurs.h
 *
 *  Created on: 07.04.2017
 *      Author: RENT
 */

#ifndef KURS_H_
#define KURS_H_
#include "Room.h"
#include <iostream>
#include <string>
class Room;
class Osoba;
class Kurs
{
public:
    Kurs();
	Kurs (std::string nazwaKursu, std::string startKurs, std::string stopKurs);
	void opis();
	void opisKursu();
	void dodajSale(Room* sala);
	void pokazSale ();
	void prowadzKurs(Osoba* profesor);
	void pokazProfesorow();
	void kursanci(Osoba* student);
	void pokazStudentow();

private:
    std::string nazwaKursu;
	std::string startKurs;
	std::string stopKurs;
	std::string lista_kursow[2];
	Osoba* lista_studentow[10];
	Osoba* lista_prowadzacych[10];
	Room* lista_sal[2];
	int liczbaSal;
	//int liczbaKursu;						// zmienne sta³e(const) z metod musze byc zdeklarowane w naglówku .h
	int liczbaProw;							//inaczej bed¹ podmienane - nadpisywane przez zmienne tymczasowe z ciala metody w pliku .cpp
	int liczbaStudentow;

};

#endif /* KURS_H_ */
