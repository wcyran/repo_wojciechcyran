#ifndef ROOM_H
#define ROOM_H
#include "Kurs.h"
#include <iostream>
#include <string>

class Room
{
    public:
        Room();
        Room(int nRoom, int pojRoom, int Building); // konstruktor musi byc taki sam w .h i .cpp
        void opis();
        //void pokazKurs();
        //void dodajKurs (std::string kurs);
        //void kursyWSali ();	// metoda zliczajaca kursu w sali przy uzyciu tablicy - powstaje zmienna nazwa kursu zaœ numer kurs u jest zliczany
        //int getNRoom ();              //uzywane tylko w main        // get zwraca nr pokoju
        //void setNRoom (int nRoom) {this ->nRoom=nRoom;}     // set wyswietla i przyjmuje atrybut wartosci nr pokoju // set przestawia zmienna
        // ustawia wartoscie w konstruktorze,
    protected:

    private:

        //int liczbaKursu;
        int nRoom;
        int pojRoom;
        int numKursuWSali;
        int Building;

        //std::string lista_kursow[20]; // tablica listy kursu do metody zliczajacej i przypisujacej nazwe i nr kursu
};

#endif // ROOM_H
