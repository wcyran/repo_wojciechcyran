/*
 * Kurs.cpp
 *
 *  Created on: 07.04.2017
 *      Author: RENT
 */
#include "Osoba.h"
#include "Kurs.h"
#include "Room.h"
#include <iostream>
#include <string>

Kurs::Kurs()
{
		liczbaProw=0;
		liczbaStudentow=0;
		liczbaSal=0;
}
Kurs::Kurs (std::string k, std::string start, std::string stop)
{
    startKurs=start;
    stopKurs=stop;
	nazwaKursu=k;
	liczbaProw=0;
	liczbaStudentow=0;
	liczbaSal=0;							// tylko w konstruktorze podajemy 0 wartosc dla tablicy
}

void Kurs::opisKursu()
{
    std::cout<<"Nazwa kursu"<<nazwaKursu<<std::endl;
    std::cout<<"Data rozpoczecia: "<<startKurs<<std::endl;
    std::cout<<"Data zakonczenia: "<<stopKurs<<std::endl;
}
void Kurs::dodajSale (Room* sala)
{
	lista_sal[liczbaSal]=sala;
	sala++;
}
void Kurs::pokazSale()
{
    for (int i=0; i<liczbaSal; i++)
    lista_sal[i]->opis();
}
void Kurs::prowadzKurs (Osoba* profesor)
{
    lista_prowadzacych[liczbaProw]=profesor;
    liczbaProw++;
}
void Kurs::pokazProfesorow()
{
    for (int i=0; i<liczbaProw; i++)
    {										//std::cout << i+1<<lista_prowadzacych[i]<<std::endl;
    	lista_prowadzacych[i]->opis();		//wykorzystujemy metode z klasy Osoba.h dodajemy jej bibliotekê
    }
}
void Kurs::kursanci(Osoba* student)
{
	lista_studentow[liczbaStudentow]=student;
	liczbaStudentow++;
}
void Kurs::pokazStudentow()
{											//int liczbaStudentow=0 ---- bedzie nam za kazdym razem zerowal wypisywanie w tablicy
	for (int i=0; i<liczbaStudentow; i++)
	{
		lista_studentow[i]->opis();      //std::cout<<i+1<<lista_studentow[i]<<std::endl; - korzystamy z metody w klasie opis w klasie Osoba
	}									// dodajemy biblioteke Osoba.h
}
void Kurs::opis()
{
	std::cout<<"**************************"<<std::endl;
	std::cout<<"Nazwa kursu: "<<std::endl;
	opisKursu ();
	std::cout<<"**************************"<<std::endl;
	std::cout<<"Prowadzacy kurs: "<<std::endl;

	pokazProfesorow();
	std::cout<<"**************************"<<std::endl;

	std::cout<<"Studenci zapisani na kurs: "<<std::endl;

	pokazStudentow();

	std::cout<<"**************************"<<std::endl;
}
