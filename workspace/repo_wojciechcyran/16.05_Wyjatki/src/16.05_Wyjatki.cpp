//============================================================================
// Name        : 05_Wyjatki.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

using namespace std;

void last ()
{
	cout<<"Last start"<<endl;

	cout<<"Throwing int exception"<<endl;
	throw -1.0;			// wyrzucamy wyjatek -1;

	cout<<"Last end"<<endl;
}

void third()
{
	cout<<"Third start"<<endl;
		last();
		cout<<"Third end"<<endl;

}
void second (){
	cout<<"Second start"<<endl;
	try
	{
		third();
	}
	catch (double)
		{
			cout<<"in second double exception"<<endl;
		}
	cout<<"Second end"<<endl;

}
void first (){
	cout<<"First start"<<endl;
	try
	{
		second();
	}
	catch (int)
	{
		cerr<<"in first int exeception"<<endl;
	}
	catch (double)
		{
			cerr<<"in first double exception"<<endl;
		}
	cout<<"First end"<<endl;

}
int main()
{
	cout << "Main start" << endl;
	try {			//blok umieszczmy czesc kodu ktora chcemy sprawdzic:
//	first ();
		int x = 2;
		int y = 0;
		if(y==0)
			throw " error dividion by 0";
		int d = x/y;

	}
	//exception -> zlapalismy wyjatek;
	catch (const char* exception) //parametr ... dziala default-owo; �apie wszystkie wyjatki; co zrobic jesli try zlapie wyjatek
	{
			cout<<"int main int exeption catched"<<exception<<endl;
	}
	cout << "Main end" << endl;
	return 0;
}
//jezeli wyjatek zostanie obsluzony gdzies wyzej to my tego wyjatku ju� nie obslugujemy
