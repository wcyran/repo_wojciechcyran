//============================================================================
// Name        : 06_Iterator.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

// istream_iterator example
#include <iostream>     // std::cin, std::cout
#include <iterator>     // std::istream_iterator
#include <vector>

int main ()
{

//	std::cout<<"iterator wejscia"<<std::endl;
//
//  double value1, value2;
//  std::cout << "Please, insert two values: ";
//
//// std::istream_iterator<double>  iterator wej�cia obslugujacy double
//
//  std::istream_iterator<double> eos;              // end-of-stream iterator
//  std::istream_iterator<double> iit (std::cin);   // stdin iterator
//
//  if (iit!=eos)
//	  {
//	  value1=*iit;
//	  ++iit;
//	  }
//  if (iit!=eos)
//	  {
//	  value2=*iit;
//	  }
//
//  std::cout << value1 << "*" << value2 << "=" << (value1*value2) << '\n';
//
//  std::cout<<"iterator wyjscia"<<std::endl;
//
//    std::vector<int> myvector;
//    for (int i=1; i<10; ++i)
//    	{
//    	myvector.push_back(i*10);
//    	}
//
//    std::ostream_iterator<int> out_it (std::cout,", ");
//
//    std::copy ( myvector.begin(), myvector.end(), out_it ); //moze byc z for ()

    std::vector<int> myvector (5);  // 5 default-constructed ints

      int i=0;

      //std::vector<int>::reverse_iterator rit = myvector.rbegin(); // inicjalizacja - tworzenie zmiennej
      for (std::vector<int>::reverse_iterator rit = myvector.rbegin(); rit!= myvector.rend(); ++rit)
      {
    	  *rit = ++i;
      }


      std::cout << "myvector contains:";
      for (std::vector<int>::iterator it = myvector.begin(); it != myvector.end(); ++it)
      {
        std::cout << ' ' << *it;
      }
        std::cout << '\n';

  return 0;
}
