/*
 * Kurs.cpp
 *
 *  Created on: 07.04.2017
 *      Author: RENT
 */
#include "Osoba.h"
#include "Kurs.h"
#include "Room.h"
#include <iostream>
#include <string>


Kurs::Kurs (std::string kurs)
{
	this->kurs=kurs;
	liczbaProw=0;
	liczbaStudentow=0;
	liczbaSal=0;							// tylko w konstruktorze podajemy 0 wartosc dla tablicy
}
void Kurs::prowadzKurs (Osoba* profesor)
{
    lista_prowadzacych[liczbaProw]=profesor;
    liczbaProw++;
}
void Kurs::pokazProfesorow()
{
    for (int i=0; i<liczbaProw; i++)
    {										//std::cout << i+1<<lista_prowadzacych[i]<<std::endl;
    	lista_prowadzacych[i]->opis();		//wykorzystujemy metode z klasy Osoba.h dodajemy jej bibliotekę
    }
}
void Kurs::kursanci(Osoba* student)
{
	lista_studentow[liczbaStudentow]=student;
	liczbaStudentow++;
}
void Kurs::pokazStudentow()
{											//int liczbaStudentow=0 ---- bedzie nam za kazdym razem zerowal wypisywanie w tablicy
	for (int i=0; i<liczbaStudentow; i++)
	{
		lista_studentow[i]->opis();      //std::cout<<i+1<<lista_studentow[i]<<std::endl; - korzystamy z metody w klasie opis w klasie Osoba
	}									// dodajemy biblioteke Osoba.h
}
void Kurs::dataRozpoczecia (int rok, int miesiac, int dzien)
{
	this->rok=rok;
	this->miesiac=miesiac;
	this->dzien=dzien;
}
void Kurs::wypiszDataRozpoczecia()
{
	std::cout<<rok<<"."<<miesiac<<"."<<dzien<<std::endl;
}
void Kurs::dataZakonczenia (int rok, int miesiac, int dzien)
{
	this->rok=rok;
	this->miesiac=miesiac;
	this->dzien=dzien;
}
void Kurs::wypiszDataZakonczenia()
{
	std::cout<<rok<<"."<<miesiac<<"."<<dzien<<std::endl;
}

void Kurs::dodajKurs(std::string kurs)
{
    lista_kursow[liczbaKursu]=kurs;
    liczbaKursu++;
}
void Kurs::pokazKurs()
{
    for (int i=0; i<liczbaKursu;i++)
    {
        std::cout << i+1<<". "<<lista_kursow[i]<<std::endl;
    }
}

void Kurs::dodajSale (Room* sala)
{
	lista_sal[liczbaSal]=sala;
	sala++;
}
void Kurs::pokazSale()
{

}
void Kurs::opis()
{
	std::cout<<"**************************"<<std::endl;
	std::cout<<"Nazwa kursu: "<<std::endl;
	pokazKurs();
	std::cout<<"**************************"<<std::endl;
	std::cout<<"Kurs prowadzony w sali : "<<std::endl;

	std::cout<<"**************************"<<std::endl;
	std::cout<<"Data rozpoczecia: "<<std::endl;

	wypiszDataRozpoczecia();
	std::cout<<"**************************"<<std::endl;
	std::cout<<"Data zakonczenia: "<<std::endl;

	wypiszDataZakonczenia();
	std::cout<<"**************************"<<std::endl;

	std::cout<<"Prowadzacy kurs: "<<std::endl;

	pokazProfesorow();
	std::cout<<"**************************"<<std::endl;

	std::cout<<"Studenci zapisani na kurs: "<<std::endl;

	pokazStudentow();

	std::cout<<"**************************"<<std::endl;
}
