/*
 * Kurs.h
 *
 *  Created on: 07.04.2017
 *      Author: RENT
 */

#ifndef KURS_H_
#define KURS_H_
#include "Room.h"
#include <iostream>
#include <string>
class Room;
class Osoba;
class Kurs
{
public:

	Kurs (std::string kurs);
	void opis();
	void dodajSale(Room* sala);
	void pokazSale ();
	void prowadzKurs(Osoba* profesor);
	void pokazProfesorow();
	void kursanci(Osoba* student);
	void pokazStudentow();
	void dataRozpoczecia (int rok, int miesiac, int dzien);
	void wypiszDataRozpoczecia ();
	void dataZakonczenia (int rok, int miesiac, int dzien);
	void wypiszDataZakonczenia ();
	void dodajKurs (std::string kurs);
	void pokazKurs();

private:

	std::string kurs;
	int dzien;
	int miesiac;
	int rok;
	std::string lista_kursow[20];
	Osoba* lista_studentow[10];
	Osoba* lista_prowadzacych[10];
	Room* lista_sal[10];
	int liczbaSal;
	int liczbaKursu;						// zmienne sta�e(const) z metod musze byc zdeklarowane w nagl�wku .h
	int liczbaProw;							//inaczej bed� podmienane - nadpisywane przez zmienne tymczasowe z ciala metody w pliku .cpp
	int liczbaStudentow;

};

#endif /* KURS_H_ */
