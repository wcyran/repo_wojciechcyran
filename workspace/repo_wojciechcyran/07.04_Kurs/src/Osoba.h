#ifndef OSOBA_H
#define OSOBA_H
#include <iostream>
#include <string>

class Osoba
{

    public:

    Osoba();
    Osoba(std::string status, std::string imie, std::string nazwisko, std::string email, int wiek);
    void dodajKurs (std::string kurs);
    void opis ();
    void pokazKurs ();

    protected:

    private:
    std::string nazwisko;
    std::string imie;
    int wiek;
    std::string email;
    std::string status;
    std::string lista_kursow[20];
    int liczbaKursu;
};

#endif // OSOBA_H
