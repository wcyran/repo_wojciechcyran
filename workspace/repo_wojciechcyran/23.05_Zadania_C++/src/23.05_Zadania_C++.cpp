//============================================================================
// Name        : 05_Zadania_C++.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include<iostream>

using namespace std;

int solution (int *A, int size_of_A)
{
	int min = 2147483647;
	if (size_of_A <10000 && size_of_A>0)
	{
		for (int i=0; i<size_of_A; ++i)
		{
			if(A[i]<min)
			min=A[i];
		}
		cout<<min;
	}
	return 0;
}

int main()
{

	int *A, N;
	cout<<"Podaj wielkosc tablicy: "<<endl;
	cin>> N;
	A = new int [N];

	for (int i=0; i<N; ++i)
	{
		cin>> A[i];
	}

	solution (A,N);

	return 0;
}
