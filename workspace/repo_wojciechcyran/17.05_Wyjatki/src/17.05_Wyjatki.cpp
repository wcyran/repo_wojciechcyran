//============================================================================
// Name        : 05_Wyjatki.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <typeinfo>
#include <fstream>

using namespace std;

class Foo{
public:
	virtual ~Foo(){

	}
};
class Bar{
public:
	virtual~Bar(){

	}
};
void displayException(exception& e){
	cout<<"exception: "<<e.what()<<endl;
}

void badException () throw (bad_exception) {
	bad_exception e;
	throw e;
}

int main()
{
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!

	Bar b;
	Bar *c= new Bar;
	try
	{
//		int *test = new int[10000000000]; 				//wywolujemy bad_alloc duza ilosc pamieci zbyt duza
//		int *test1 = new (nothrow) int[10000000000]; 	//bez wyjatku
//		if (test== 0){
//			cout<<"nullptr"<<endl;
//		}
//		Foo &f = dynamic_cast<Foo&> (b);  				//wyrzuca bad_cast

//		bad_typeid();
//		cout<<typeid(*c).name()<<endl;
		string text;
		text.at(2); //docieramy do 2 znaku zmiennej string i wyrzuca na out of range
//		ifstream f("moj plik nie istenie");
//		f.exceptions (f.failbit);
		string x;
		x.resize((x.max_size()+1); //

	}
	catch (bad_alloc& e){ //
		displayException(e);
		}
	catch (bad_cast& e){
		displayException(e);
	}
	catch (bad_exception& e){ //throw
		displayException(e);
		}
	catch (bad_typeid& e){
		displayException(e);
		}
	catch (ios_base::failure& e){
		displayException(e);
			}
	catch (length_error& e){
			displayException(e);
				}
	catch (logic_error& e){
		//invalid argument;
		//domain error;
		//length_error;
			cout<<"logic_error"<<endl;
			displayException(e);
				}
	catch (out_of_range& e){		//dziedzicza po bledzie logic error
			displayException(e);
				}
	catch(...){									//deufult typ
		cout<<"unrecognized exception"<<endl; 	//gotowa metoda systemowa
	}
	return 0;
}
