//============================================================================
// Name        : 05_Static_cw.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "X.h"

//--------Singleton----------//

// nie mozemy utworzyc obiektu ale mozemy utworzyc wskaznik na zmienna a klasy X;

using namespace std;

int main()
{
	X &a=X::getInstance(); //referencja zawsze musi byc zainicjalizowana i zawsze tam musi byc jakis obiekt
	a.getAInt();
	return 0;
}
