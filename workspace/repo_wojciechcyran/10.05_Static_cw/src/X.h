/*
 * X.h
 *
 *  Created on: 10.05.2017
 *      Author: RENT
 */

#ifndef X_H_
#define X_H_

class X //atrybut musi przechowywac 2 rzeczy
		//utworzenie obiektu kuiedy metoda jest wywo�ana 1 raz
{
public:
	int getAInt() {return 1;}
	virtual ~X();
			//stala na referencje
	static X& getInstance(); //metoda statyczna ktora zwroci nam wskaznik lub referencje bo nie zabiera miejsca
							// idea jest taka zeby nie tworzyc wiecej niz 1 obiekt
							// medoda statyczna jest metoda ktora mozemy wywolac nie majac obiektu
private:
	static X* instance;
	X();
	X (const X&);
};

#endif /* Singleton_H_ */

//
