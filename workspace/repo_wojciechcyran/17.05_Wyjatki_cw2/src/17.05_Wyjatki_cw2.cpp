//============================================================================
// Name        : 05_Wyjatki_cw2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <typeinfo>

using namespace std;

int *allocateIntArray (int n)
{
	int *x = new int [n];
	return x;
}
int *allocateIntArrayNoThrow (int n)
{
	int *x = new (nothrow) int [n];
	return x;
}

int main()
{
	cout << "!!!Hello World!!!" << endl;
	int *firstArray;
	int *secondArray;
	int n,m;
	cout<<"podaj liczbe 1:"<<endl;
	cin>>n;
	cout<<"podaj liczbe 2:"<<endl;
	cin>>m;
		try
		{
			firstArray = allocateIntArray(n);
			secondArray = allocateIntArrayNoThrow(m);
			if(!secondArray){
				bad_alloc e;
				throw e;
			}

		}
		catch (bad_alloc& e)
		{
			cout<<"bad_alloc "<<e.what()<<endl;
		}

		catch(...){									//deufult typ
			cout<<"unrecognized exception"<<endl; 	//gotowa metoda systemowa
		}

	return 0;
}
// funkcja bedzie przyjmowac liczbe dodatnia,
// bedzie alokowac tablice intow, bedzie zwracac wartosc wskaznik ado pierwszego elementu
