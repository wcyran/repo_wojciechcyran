//============================================================================
// Name        : Sort_cw2_przez_wstawianie.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int solution(int *A, int size_of_A) {
	int min = 2147483647;
	if (size_of_A < 100000 && size_of_A > 0) {
		for (int i = 0; i < size_of_A; i++) {
			if (A[i] < min) {
				min = A[i];
			}
		}
		return min;
	} else {
		cout << "nieprawidłowy rozmiar tablicy" << endl;
		return 0;
	}
}

int main() {
	int tablica[5] = { 0, 6, 2147483647, -2147483647, 12 };
	cout << solution(tablica, 5) << endl;
	cout << solution(tablica, 0) << endl;
	cout << solution(tablica, 200000000) << endl;
	cout << solution(tablica, 100) << endl;
	cout << solution(tablica, 4) << endl;
	return 0;
}


































#include <iostream>
using namespace std;


void sortowaniePrzezWstawianie(int n, int *tab)
{
int i=0;
int j=0;
int key;

	for(i=1; i<n; i++)
	{
		key = tab[i];
		j=i-1;
		while (i>0 && tab[i]>key)
		{
			tab[j+1]=tab[j];
			--j;
		}
		tab[j+1]=key;
	}
}

int main()
{
	int *tab, n;
	 cout<<"Podaj wielkość zbioru: "<<endl;
	 cin>>n;

	 tab= new int [n];
	 for (int i=0; i<n;i++)
	 {
		 cout<<"Podaj: "<<i+1<<" zbioru. "<<endl;
		 cin>>tab[i];
	 }
	 cout<<"Elementy przed sortowaniem: "<<endl;
	 for (int i=0; i<n; i++)
		 cout<<tab[i]<< " ";

	 sortowaniePrzezWstawianie(n, tab);

	 cout<<"Elementy posortowane: "<<endl;
	 for (int i=0; i<n; i++)
		 cout<<tab[i]<<" ";
	 delete [] tab;

	return 0;

}
