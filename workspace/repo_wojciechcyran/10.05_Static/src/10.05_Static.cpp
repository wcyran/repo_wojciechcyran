//============================================================================
// Name        : 05_Static.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Class.h"

using namespace std;

int global=0;

void f()
{
	static int s=0;				//licznik wywolan funcji (counter)
	//static const int s = 0; 	//stala
	int a =0;

	std::cout <<" a wynosi: "<<++a<<"\t";
	std::cout<< "Funkcja wywolana " <<++s <<" razy"<<std::endl;
	//std::cout<< "Funkcja wywolana " <<++global <<" razy";

}

int main()
{
	int global=5;
	static int s=4;
	for (int i=0; i<10; ++i)
	{
		f();
		s+=5;
	}
	std::cout<<global<<std::endl;

	std::cout<<"-----KLASA------"<<std::endl;
	Class aa;

	aa.printA(5);

	//Class::r=55; // metoda czy zmienna statyczna nie ma powiązania z obiektem klasy;

	//aa.printR();

	Class::printRR();

	aa.getR();
	//aa.printR();

	//std::cout<<r<<std::endl;
//	for (int i=0; i<10; ++i)
//		{
//			f();
//			global+=5;
//		}

	return 0;
}
