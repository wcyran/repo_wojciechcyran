/*
 * Class.h
 *
 *  Created on: 10.05.2017
 *      Author: RENT
 */

#ifndef CLASS_H_
#define CLASS_H_

class Class
{

public:

	Class();
	virtual ~Class();

	void printA (int a);
	int getA()		//getter zwykle ma const bo nie zmienia obiektu
	{
		return a;
	}

	void setR(int r)
	{
		this->r = r;
	}

	int getR ();
	void printR (int rr);

	static void printRR (); // metoda

private:
	static int r;
	int a;

private:

};

#endif /* CLASS_H_ */
