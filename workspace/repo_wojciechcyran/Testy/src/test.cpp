/*
 * test.cpp
 *
 *  Created on: 21.04.2017
 *      Author: RENT
 */
#include "../gtest/gtest.h"
#include "functions.h"
#include "TabTest.h"

TEST (TestingPowers, Four)
{
	EXPECT_EQ(100,secondPower(10));
}

TEST (SuperTab, SpeawdzamyCzyjestJedenElemWTablicy)
{
	TabTest Tab;
	EXPECT_EQ (1, Tab.returnSize());
}
TEST (SuperTab, SprawdzamyCzyDodajeWiecejElem)
{
	TabTest Tab;
	EXPECT_EQ (1, Tab.returnSize());
	Tab.addElement(12);
	Tab.addElement(13);
	Tab.addElement(15);
	EXPECT_EQ (4, Tab.returnSize());
}
TEST (SuperTab, ShowElement_PossitiveTab0)
{
	TabTest Tab;

	Tab.addElement(15);
	Tab.addElement(2);
	Tab.addElement(3);
	EXPECT_EQ (123, Tab.show_Element(0));
}
TEST (SuperTab, ShowElement_PossitiveTab1)
{
	TabTest Tab;

	Tab.addElement(89.9);
	Tab.addElement(9);
	Tab.addElement(5);
	EXPECT_EQ (89.9, Tab.show_Element(1));// 1 element tablicy to 12
}
//TEST (SuperTab, addElement_PossitiveTab2)
//{
//	TabTest Tab;
//	EXPECT_EQ(2.5, Tab.addLastElement(2.5));
//}

TEST (SuperTab, minusElement_PossitiveTab1)
{
	TabTest Tab;
	Tab.addElement(89.9);
	Tab.addElement(9);
	Tab.addElement(9);
	Tab.minusElement();
	Tab.minusElement();
	EXPECT_EQ (2, Tab.returnSize());  // 1 element tablicy to 12
}

TEST (WskaznikTab, pointerElement_PossitiveWSK)
{
	TabTest Tab;
	Tab.addElement(89.9);
	EXPECT_EQ (123, Tab.pointerFirstElement ());
	// trzeba pobrac wartosc wskaznika
}

//TEST (testSilnia, POSSITIVE)
//{
//	EXPECT_EQ(2, obl_silnia (2));
//}

