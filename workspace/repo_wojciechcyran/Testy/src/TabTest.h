/*
 * TabTest.h
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#ifndef TABTEST_H_
#define TABTEST_H_

const int TabSize = 1000;
class TabTest
{
private:

	double Tab [TabSize];
	int number_of_elements;
	int elementIndeks;

	//int indeksNumber;

public:

	void addElement (double element);

	double show_Element (int elementIndeks); //zwraca element z danego indeksu

	int returnSize ();

	int addLastElement (int element);

	void minusElement ();

	double * pointerFirstElement ();

	//int minusLastElement (int element);


//	void substractElement (double element);

//	int zwraca_ilosc_elementow ();
//	int zwraca_element_z_indeksu ();
//	int zwraca_wskaznik_na_poczatkowy_elem ();
//
//	get dodaj_na_koniec ();


	TabTest();
	virtual ~TabTest();
};

#endif /* TABTEST_H_ */
