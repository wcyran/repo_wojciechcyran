//============================================================================
// Name        : 03_Klasy_konstruktor_destruktor.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "klasy.h"
using namespace std;

int main()
{
    Event w1; // ("Kawalerski",6,8,2017,8,15); // konstruktor zostaje automatycznie wywolany !!!
    		  // Musi miec przypisane wartosci domyslne - STARTOWE  !!!
              // mozemy wprowadziæ metody get i set
    		  //w1.getEvent(); // w funkcji main wywolujemy metode wpisywania
    w1.setEvent(); // ------ || ----- wywolujemy metode wyswietlania;
    /*w1.day=02;
    w1.month =03;
    w1.year=1999;
    cout<<w1.day<<" "<<w1.month<<" "<<w1.year;*/
    return 0;
}

void Event::getEvent()
{
    cout<< endl << "Nazwa wydarzenia: ";
    cin >> name;
    cout<< endl << "Dzien: ";
    cin >> day;
    cout<< endl << "Miesiac: ";
    cin >> month;
    cout<< endl << "Rok: ";
    cin >> year;
    cout<< endl << "Godzina: ";

    cin >> hour;
    cout<< endl << "Minut: ";
    cin >> minutes;
}
void Event::setEvent()
{
    cout<<endl<<name<< "    "<<endl<<day<<"."<<month<<"."<<year<<"  "<<endl<<hour<<" : "<<minutes<<endl;

}
//konstruktor
Event::Event (string n, int d, int m, int y, int h, int minut) //klasa operator zasiegu metoda
{
    name=n;
    day =d;
    month=m;
    year=y;
    hour=h;
    minutes=minut; // ustawiamy nowe atrybuty dla wartosci
}
//destruktor
Event::~Event()
{
    cout << endl <<"Destruktor !!!";
}
