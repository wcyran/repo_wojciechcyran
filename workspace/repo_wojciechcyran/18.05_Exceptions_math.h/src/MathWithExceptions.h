/*
 * MathWithExceptions.h
 *
 *  Created on: 18.05.2017
 *      Author: RENT
 */

#ifndef MATHWITHEXCEPTIONS_H_
#define MATHWITHEXCEPTIONS_H_
#include <math.h>
#include <exception>


class MathWithExceptions
{
public:
	class DomainException : public exception{
		DomainException (string value);
		virtual const char* what () const throw (){
			return " ";
		}
		const string exception_type;
		MathWithExceptions ();
		virtual ~MathWithExceptions ();



	double pow (double x, double y);
	double log (double value);
	double cos (double angle);
	double sin (double angle);
	double tan (double angle);
private:
	double x, y, value;
	double angle;
};

#endif /* MATHWITHEXCEPTIONS_H_ */
