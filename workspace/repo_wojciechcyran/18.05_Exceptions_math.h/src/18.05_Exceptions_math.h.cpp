//============================================================================
// Name        : h.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <math.h>
#include <exception>
#include "MathWithExceptions.h"

using namespace std;

//namespace math
//{
//	struct mathM
//	{
//		int x, y;
//
//	};
//}

void displayException(exception& e){
	cout<<"exception: "<<e.what()<<endl;
}

int main()

{
	try
	{
	math::mathM dane;
	dane.x=3;
	dane.y=100000000000000000000;



	}
	catch (domain_error& e){
		displayException(e);
	}
	catch (range_error& e){
		displayException(e);
	}
	catch(...){

	cout<<"unrecognized exception"<<endl;
	}
	return 0;
}

//1. klasa MathWithExceptions bedaca wrraperem (okala -> klasa w niej metody z math.h i rzucaja wyjatki) dla biblioteki math.h
//2. MathWithExceptions posiada metody :
// log, pow, cos, sin, tan,
//3. Klasa posiada wlasny typ wyjatkow: Domain error, Range error,
//4. Wyjatek powinien sugerowac rozwiazanie Range -> liczba jest za ma�a
//5. Wykorzystanie namespace, w�asny "math"
//6. mo�na wykorzystywac metody klasy bez obiektu,
