
#ifndef LOGWRITER_H_
#define LOGWRITER_H_
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

class LogWriter
{
public:
	LogWriter ();

	virtual ~LogWriter();

	enum LogType {Fatal, Error, Warning, Info, Debug, Trace}; // wiadomosci roznych typoow

	void logMessage (string message, LogType messageType);
	void setLogginLevel(LogType logLevel);

private:

	fstream f;
	LogType logLevel;
};

#endif /* LOGWRITER_H_ */
