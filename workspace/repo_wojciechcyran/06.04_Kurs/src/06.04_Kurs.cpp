//============================================================================
// Name        : KlasyKursy.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include "sala.h"
#include "osoba.h"

using namespace std;


int main()
{

	osoba tab_osob[10];

	std::cout<<"------Studenci-------"<<std::endl;
	tab_osob[0]=osoba("Student","Karol","Zdeb","zdeb@gmail.com", 24);
	tab_osob[1]=osoba("Student", "Ela", "Fula","ela@gmail", 34);
	tab_osob[2]=osoba("Student","Wojciech", "Cyran", "wcyran@gmail.com", 29);

	std::cout<<"------Profesorzy-------"<<std::endl;

	tab_osob[3]=osoba("Profesor","Jerzy","Grebosz","jerzy@gmail.com", 65);
	tab_osob[4]=osoba("Profesor","Jerzy","Grebosz","jerzy@gmail.com", 65);

		for (int i=0; i<10; i++)
		{
			tab_osob[i].describe();
		}
		return 0;

	//tab_sala[0].describe()
	std::cout<<"----------------------------"<<std::endl;




	/*osoba s3 ("Student", "Ela", "Fula","ela@gmail", 34);
	s3.addCourse("Geografia");
	s3.describe();
	osoba s1("Student","Karol","Zdeb","zdeb@gmail.com", 24);
	s1.addCourse("Fizyka");
	//s1.showCourses();
	s1.describe();
	osoba s2("Student","Wojciech", "Cyran", "wcyran@gmail.com", 29);
	s2.addCourse("Informatyka");
	//s2.showCourses();
	s2.describe();*/


	osoba p1("Profesor","Jerzy","Grebosz","jerzy@gmail.com", 65);
	p1.addCourse("C++");
	//p1.showCourses();
	p1.describe();

	osoba p2("Profesor", "Jacek", "Rawski", "j.rawski@gmail.com",56);
	p2.addCourse("Matematyka");
	p2.describe();

	//tablica sal//

	std::cout <<"----Tablice sal-----"<< std::endl;

	sala tab_sala[10];

	tab_sala [0]= sala(1,1,1);
	tab_sala[0].describe();

	std::cout <<"--------------"<< std::endl;

	sala sala1 (10,35,1);
	sala1.describe();


	sala sala2 (11,36,2);
	sala2.describe();

	//rejstracja//

	//A tablicaObiektow[10]={}; //tablica obiektow
	//A *tablicaWsk [10]; tablica wskaznikow

	return 0;
}

