/*
 * osoba.h
 *
 *  Created on: 06.04.2017
 *      Author: RENT
 */

#ifndef OSOBA_H_
#define OSOBA_H_
#include <iostream>
#include <string>

class osoba
{
public:
	osoba ();
	osoba(std::string status, std::string name, std::string surname, std::string email, int age);
	void describe();
	void showCourses();
	void addCourse(std::string course);

	int getAge() const {return age;}
	void setAge(int age) {this->age = age;}

	const std::string& getEmail() const {return email;}
	void setEmail(const std::string& email) {this->email = email;}

	const std::string& getName() const {return name;}
	void setName(const std::string& name) {this->name = name;}

	const std::string& getSurname() const {return surname;}
	void setSurname(const std::string& surname) {this->surname = surname;}

private:
	std::string status, name, surname, email;
	int age, numCourses;
	std::string courseList[20];

};

#endif

