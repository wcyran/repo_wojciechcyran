
#ifndef SALA_H_
#define SALA_H_

#include <string>
#include <iostream>

class sala
{
public:
	sala();
	sala(int numRoom, int capacity, int building);
	void describe();
	void addCourse(std::string course);
	void coursesInRoom();

	int getBuilding() {return building;}
	void setBuilding (char building) {this->building = building;}
	int getCapacity() const {return capacity;}
	void setCapacity(int capacity) {this->capacity = capacity;}
	int getNumRoom() const {return numRoom;}
	void setNumRoom(int numRoom) {this->numRoom = numRoom;}

private:

	int numRoom, capacity, numCoursesInRoom;
	int building;
	std::string courseList[20];

};
#endif
