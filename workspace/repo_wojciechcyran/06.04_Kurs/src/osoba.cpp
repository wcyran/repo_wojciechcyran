#include "osoba.h"
#include <string>
#include <iostream>


osoba::osoba()
{
}
osoba::osoba(std::string status, std::string name, std::string surname, std::string email, int age)
{
	this->status=status;
	this->name=name;
	this->age=age;
	this->email=email;
	this->surname=surname;
	numCourses=0;
}
void osoba::addCourse(std::string course)
{
	courseList[numCourses] = course;
	numCourses++;
}
void osoba::showCourses()
{
	for (int i = 0; i < numCourses; i++) {
			std::cout << i + 1 << ". " << courseList[i] << std::endl;
		};
}
void osoba::describe()
{
	std::cout<<"**************************"<<std::endl;
	std::cout<<"Status :"<<status<<std::endl;
	std::cout<<"Imie: "<<name<<std::endl;
	std::cout<<"Nazwisko: "<<surname<<std::endl;
	std::cout<<"Wiek: "<<age<<std::endl;
	std::cout<<"Adres e-mail: "<<email<<std::endl;
	std::cout<<"Kurs: "<<std::endl;
	showCourses();
}

