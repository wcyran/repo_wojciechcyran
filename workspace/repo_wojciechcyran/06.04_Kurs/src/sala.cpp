/*
 * sala.cpp
 *
 *  Created on: 06.04.2017
 *      Author: RENT
 */

#include "sala.h"
#include <iostream>
#include <string>


sala::sala(): numRoom (-1), capacity (-1), building (-1)
{
}
sala::sala(int numRoom, int capacity, int building)
{
	this->building=building;
	this->capacity=capacity;
	this->numRoom=numRoom;
	numCoursesInRoom=0;

}
void sala::addCourse(std::string course)
{

	courseList[numCoursesInRoom] = course;
	numCoursesInRoom++;
}
void sala::describe()
{
	std::cout<<"**************************"<<std::endl;
	std::cout << "Numer sali: "<<numRoom<<std::endl;
	std::cout<< "Ilosc miejsc: "<<capacity<<std::endl;
	std::cout<< "Budynek: "<<building<<std::endl;
	std::cout<<"Kursy w sali: "<<std::endl;

	coursesInRoom();
}
void sala::coursesInRoom() {
	for (int i = 0; i < numCoursesInRoom; i++) {
		std::cout << i + 1 << ". " << courseList[i] << std::endl;
	};

}

