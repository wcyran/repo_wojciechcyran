//============================================================================
// Name        : Wskaznik_cw_2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

//Program wczytujacy podana ilosc liczb i wypisujacy je na ekran

#include <iostream>
using namespace std;

const int max_array_size = 5; // stala zadeklarowana - romiar tablicy musi byc znany
int t[max_array_size];

int main()
{

	for (int i=0;i<max_array_size;++i)
	{
		cin >> t[i];		// temp = 0
	}

	int *itab=t;

	for (int i=0;i<max_array_size;++i)
	{
		cout << *itab++ << "  ";
	}
	return 0;
}
