/*
 * student.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef STUDENT_H_
#define STUDENT_H_

#include <iostream>
#include <string>
#include "sala.h"

class student {
public:
	std::string kurs;

	void kursyStudenta ();		//kursy studenta w ktorych uczestniczy
	void zapiszNaKurs();		//zapisz na kurs (tematyka)
	void opis();  				//opis ??

	//student(std::string imie="Wojtek", std::string nazwisko="Cyran", double wiek=29, std::string email="wcyran");

	int setStudent (std::string imie, std::string nazwisko, double wiek, std::string email);


private:
	std::string imie;
	std::string nazwisko;
	double wiek;
	std::string email;

};

#endif /* STUDENT_H_ */
