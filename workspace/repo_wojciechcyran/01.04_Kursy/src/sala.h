/*
 * sala.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef SALA_H_
#define SALA_H_
#include <iostream>
#include <string>

class sala
{
public:



	void opisSali (int nrSali, double pojSali, std::string salaBudynek, std::string kursy);
	void dodajKurs ();
	void kursWSali ();

	sala();
private:

	int nrSali;
	double pojSali;
	std::string salaBudynek;
	std::string kursy;

};

#endif /* SALA_H_ */
