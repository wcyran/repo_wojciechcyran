/*
 * MapDisplayer.h
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef MAPDISPLAYER_H_
#define MAPDISPLAYER_H_

#include "Map.h"
class MapDisplayer {
public:
	MapDisplayer();
	virtual ~MapDisplayer();
	void setMap (Map* _map);
	void displayMap();

private:
	Map *map;//wskaznik na obiekt typu map
};

#endif /* MAPDISPLAYER_H_ */
