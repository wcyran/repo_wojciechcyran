/*
 * Enemy.h
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef ENEMY_H_
#define ENEMY_H_

#include "GameObject.h"

class Enemy: public GameObject {
public:
	Enemy();
	virtual ~Enemy();
	char getSymbol (); //dzeidziczymy z kalsy GameObject

};

#endif /* ENEMY_H_ */
