/*
 * MapDisplayer.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#include "MapDisplayer.h"
#include <iostream>

MapDisplayer::MapDisplayer()
{

map = nullptr;
}

MapDisplayer::~MapDisplayer() {

}

void MapDisplayer::setMap(Map* _map)
{
	map = _map;
}

void MapDisplayer::displayMap()
{
	if (map==nullptr)
	{
		return;	//chcemy zeby nic sie nie robilo
	}
	for (int i=0; i<25; i++){
		for (int j=0; j<25;j++)
		{
			std::cout<<map->getPoint(j,i);//wspolrzedne j odpowiada za x, j=y wyswietlane
		}
		std::cout<<std::endl;
	}
}
