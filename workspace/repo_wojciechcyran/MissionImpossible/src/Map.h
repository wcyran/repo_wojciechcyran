/*
 * Map.h
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef MAP_H_
#define MAP_H_

#include "GameLogic.h"

class Map // przechowywujemy tab char ma symbole, generujemy mape
{
public:
	Map();
	virtual ~Map();

	void generateMap(); //generuje mape
	char getPoint (int x, int y);	//nie ma problemu ze wskaznikami (podajemy 0,0)

	void setObjectList(GameObject** _objectList)//podwojny wskaznik -> wskaznik na wskaznik
	{
		object_list = _objectList;
	}

private:

	char map[25][25];
	GameObject **object_list;
};

#endif /* MAP_H_ */
