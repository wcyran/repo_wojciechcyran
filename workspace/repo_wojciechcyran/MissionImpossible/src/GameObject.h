/*
 * GameObject.h
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef GAMEOBJECT_H_
#define GAMEOBJECT_H_

class GameObject {
public:


	GameObject();
	virtual ~GameObject();

	virtual char getSymbol ();//bedziemy dziedziczyc trzeba podmienic symbol dla danego obiektu

// do ustawiania warosci
	int getX() const //source generate getters and setters;
	{
		return x;
	}

	int getY() const {
		return y;
	}

	void setX(int x) {
		this->x = x; // mozna dac inna nazwe
	}

	void setY(int y) {
		this->y = y;// mozna dac inna nazwe
	}

private:
	int x,y;
};

#endif /* GAMEOBJECT_H_ */
