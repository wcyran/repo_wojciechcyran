//============================================================================
// Name        : MissionImpossible.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "KeyManager.h"
#include "Map.h"
#include "MapDisplayer.h"
#include "GameLogic.h"
#include "GameObject.h"

using namespace std;

void printHelp ()
{
	cout<<"Help: "<<endl<<"o  - exit "<<endl<<"r  - idle "<<endl<<"w  - up"<<endl<<"s  - down"<<endl<<"a  - left"<<endl<<"d  - right"<<endl;
}

int main()
{
	bool exit = false;
	//utworzenie obiektu keyManager
	KeyManager 	*key_manager = new KeyManager;		//przekazujemy wskaznik na obiekt// przechwytywanie klawiszy
	GameLogic * game_logic = new GameLogic;			//utworzenie obiektu logiki
	Map *map = new Map; 							//utworzenie obiektu mapy
	MapDisplayer *map_displayer = new MapDisplayer; //utworzenie obiektu wyswietlajacego mape
	map_displayer->setMap(map);
	map->setObjectList(game_logic->getObjectList());
	game_logic->setKeyManager(key_manager);

						//klasa //konwenecje znakow przydaje sie przy przepisywaniu kodu (refaktoryzacje zmiennych)
						//inicjalizacja nowego obiektu
						//utworzenie obiektu logiki
						//petla glowna sprawdza czy gra jest skonfigurowana (czy mamy w mapie ustawione wszystko zwiazane z iloscia graczy wielkoscia mapy)
						//popros gracza o kolejny znak;
						//czytamy kolejny znak;

	game_logic->configureGame();
	map->generateMap();				//generowanie mapy --
	map_displayer->displayMap();

	while (exit==false)
	{

		char key;
		cout<<"Please enter key and press enter. (h for help)"<<endl;
		cin >> key;
		if(key=='h')
		{
			printHelp ();
			continue; //po wypisaniu help przejdzie dalej
		}

		key_manager->addNewKey(key);	//tworzymy obiekt mapy -- mozna rozbudowac -- zapamietywanie
		if(game_logic->nextTurn())
		{
			map->generateMap();				//generowanie mapy --
			map_displayer->displayMap();
			exit =true;
			cout<<"GAME OVER !!!"<<endl;
			cout<<"GotCHA !!!"<<endl;
			continue;
		}
		map->generateMap();				//generowanie mapy --
		map_displayer->displayMap();
									//logika gry
			//wyswietlanie mapy

		//exit = true;
	}
	delete key_manager;//usuniecie obiektu keyManager
	delete map;//usuniecie obiektu logiki
	delete map_displayer;//usuniecie obiektu mapy
	delete game_logic;
	cout<<"Game ended succesfull"<<endl;
	return 0;
}
