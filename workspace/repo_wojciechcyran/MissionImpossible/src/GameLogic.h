/*
 * GameLogic.h
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef GAMELOGIC_H_
#define GAMELOGIC_H_
#include "Player.h"
#include "Enemy.h"
#include "KeyManager.h"


class GameLogic {
public:


	GameLogic();
	virtual ~GameLogic();
	void configureGame();
	bool nextTurn ();//zwraca informacje nt. konca gry
	void randomPosition (GameObject *object); //wybiera jak�kolwiek pozycje
	bool checkPosition (int x, int y);

	bool checkPlayerPosition();

	GameObject** getObjectList()
	{
		return object_list;//zwracanie i-tego elementu  object_list (wraz ze sprawdzeniem czy niewychodzi poza zakres);
	}

	void setKeyManager(KeyManager*& keyManager) {
		key_manager = keyManager;
	}

private:
	void updatePlayerPosition();
	KeyManager *key_manager;
	Player *player;	//wskaxniki
	Enemy *enemies [5]; //lista przeciwnikow
	GameObject *object_list [6]; //tutaj jest wpisany player i wszyscy enemy
								//object list bedziemy przekazywac do mapy. Mamy te same wskaxniki
};

#endif /* GAMELOGIC_H_ */
