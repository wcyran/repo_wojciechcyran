/*
 * GameLogic.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */
#include "stdlib.h"
#include "time.h"
#include "GameLogic.h"
#include "math.h"

GameLogic::GameLogic() {

	player= nullptr;
	for (int i=0; i<5; i++)
			{
				enemies [i]= nullptr;
			}
	srand (time(NULL)); // zmienne losowe

}

GameLogic::~GameLogic()
{
	if (player!=nullptr) //nie kasuje
	{
		delete player;
	}
	for (int i=0; i<5; i++)
			{
				if(enemies [i] != nullptr)
					{
					delete enemies[i];
					}
			}
}
void GameLogic::configureGame()
{
	if (player!=nullptr)
		{
			delete player;
		}
	for (int i=0; i<5; i++)
			{
				if(enemies [i] != nullptr)
					{
					delete enemies[i];
					}
			}

	player = new Player;
	object_list [0]= player; // zapisalismy playera

	for (int i=0; i<5; i++)
	{
		enemies [i]= new Enemy;
		object_list [i+1]= enemies [i]; // lecimy od i +1
	}

	for (int i=0; i<6; i++)//przypisanie losowych pozycji playera i enemies

	{
		randomPosition(object_list[i]);
	}
	while (checkPlayerPosition()==false)
	{
		randomPosition(player);
	}
}

bool GameLogic::nextTurn()
{
	if(key_manager->getNextKey()=='o')
	{
		return true;
	}
	updatePlayerPosition();

	//wygenerowanie nowej pozycji gracza
	//wygenerowanie nowej pozycji przeciwnikow
	checkPlayerPosition(); //sprawdzanie czy jakikolwiek przeciwnik jest w odl. jednego pola od gracza
	//zwraca true - gra zostanie zakonczona;
	if (checkPlayerPosition()==false)
	{
		return true; // true to zakonczenie gry;
	}
	return false;
}

void GameLogic::randomPosition(GameObject* object) // podajemy jak�s pozycje
{
	//wygererujemy i nast�pnie sprawdzimy pozyje
	int x,y;
	bool success = false;
	while (success == false)
	{
		x= rand () % 25; 	//generujemy zmienne losowe (taka jest konstrukcja
		y= rand () % 25;	//zwraca od 0 do 24
		if (checkPosition(x,y)) //if true to mo�emy przypisac pozycje
		{
			object-> setX(x);
			object-> setY(y);
			success = true;
		}
	}
}
bool GameLogic::checkPosition(int x, int y) //sprawdzamy czy ktos tam ju� nie stoi
{
	for (int i=0;i<6; i++)
	{
		if(object_list[i]->getX() == x && object_list[i]->getY()==y)
		{
		return false;
		}
	}
	return true; //pozycja pusta
}

bool GameLogic::checkPlayerPosition() //czy player jest w ktorejkolwiek z kratek obok przeciwnika
{
	for (int i=1;i<6; i++) 			//jak true to pozycja jest ok. false oznacza przegrana
		{
			if(abs (object_list[i]->getX() - player->getX())<=1 && abs (object_list[i]->getY() - player->getY())<=1) //abs -> wartosc bezwzgladna
			{
			return false;
			}
		}
	return true;
}

void GameLogic::updatePlayerPosition()
{
	switch (key_manager->getNextKey())
	{
		case 'w':
		if(player->getY()-1>=0)
		{
			player->setY(player->getY()-1);
			break;
		}

	case 's':
		if(player->getY()+1<25)
		{
					player->setY(player->getY()+1);
				break;
		}


	case 'a':
			if(player->getX()-1>=0)
			{
				player->setX(player->getX()-1);
			}
			break;
	case 'd':
			if(player->getX()+1<25)
			{
						player->setX(player->getX()+1);
					break;
			}

	}
}
