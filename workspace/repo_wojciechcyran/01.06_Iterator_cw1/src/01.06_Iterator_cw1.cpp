//============================================================================
// Name        : 06_Iterator_cw1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <iterator>

using namespace std;

int main()
{
	cout<<"Wpisz 5 liczb: "<<endl;
	int myint;
	int temp;
	int x=100;
	std::vector<int>myvector;

	for(int i=0; i<5;++i) 			//myvector.size()
	{
		cin>>myint;
		myvector.push_back(myint);
	}

	cout<<"Wypisywanie od poczatku do konca: "<<endl;
	for(std::vector<int>::iterator it = myvector.begin(); it!=myvector.end();++it)
	    {
	        cout<<' '<< *it;
	    }
	    cout<<endl;
	cout<<"Wypisywanie od konca: "<<endl;
	for(std::vector<int>::iterator it = myvector.end()-1; it!=myvector.begin()-1;--it)
	    {
	    	cout<<' '<< *it;
	    }
	    	cout<<endl;
	   cout<<"Mnozenie od 2 elementu w tablicy: "<<endl;
	   for(std::vector<int>::iterator it = myvector.begin()+2; it!=myvector.end();++it)
	   {
		   temp=(*it)*(*it);
		   cout<<' '<< temp;
	    }
	    cout<<endl;

	 std::ostream_iterator<int> out_it (std::cout, ", ");
	 std::copy (myvector.begin(),myvector.end(),out_it);
	 cout<<endl;
	 cout<<"Druga tablica: "<<endl;
	 std::vector<int>myvector2;
	 cout<<endl;
	 	for(int i=0; i<10;++i) 			//myvector.size()
	 	{
	 		++x;
	 		myvector2.push_back(x);
	 	}
	 	for(int i=0; i<myvector2.size(); ++i)
	 	{
	 		cout<<myvector2[i]<<' ';
	 	}
	 	cout<<endl;
	 cout<<"I. Kopiowanie elementu z II do I tablicy: "<<endl;

	 //	std::ostream_iterator<int> out_it (std::cout, ", ");

	std::copy (myvector.begin()+2,myvector.end(),myvector2.begin()+2);//kopiowanie danych w tablicy

	for(int i=0; i<5;++i) 			//myvector.size()
		{
			cout<<myvector2[i]<<' ';
		}
	cout<<endl;


	cout<<"II.Kopiowanie elementu z II do I tablicy: inserter "<<endl;



	  std::insert_iterator< std::vector<int> > insert_it (myvector2,myvector2.begin());

	  std::copy (myvector.begin(),myvector.end(),insert_it);

//	  for(int i=0; i<10;++i) 			//myvector.size()
//	  		{
//	  			cout<<myvector2[i]<<' ';
//	  		}
//	  	cout<<endl;
	  for(std::vector<int>::iterator it = myvector2.begin(); it!=myvector2.end();++it)
	  		    {
	  		        cout<<' '<< *it;
	  		    }


	return 0;
}
