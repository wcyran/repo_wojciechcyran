//============================================================================
// Name        : Kalkulator.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cmath>

using namespace std;

int a;
int b;
int wybor;

int main()
{
	cout << "Kalkulator - Menu glowne" << endl;
	cout << "--------------------------" << endl;
	cout << "1. Dodawanie" << endl;
	cout << "2. Odejmowanie" << endl;
	cout << "3. Mnozenie" << endl;
	cout << "4. Dzielenie" << endl;

	cout <<" Podaj pierwsza liczbe: "<< endl;
	cin >>a;
	cout << "Podaj druga liczbe: " << endl;
	cin >>b;
	cout << "Wybierz dzialanie: " << endl;
	cin >> wybor;

	if      (wybor == 1)
			cout <<"Wynik dodawania to " << (a+b) << endl;
	else if (wybor == 2)
			cout <<"Wynik odejmowania to " << (a-b) <<endl;
	else if (wybor == 3)
			cout <<"Wynik mnozenia to " << (a*b) << endl;
	else if (wybor == 4)
			cout <<"Wynik dzielenia to " << (a/b) << endl;
	else
			cout <<"Podana liczba jest bledna " << endl;
	return 0;
}
