/*
 * NumID.h
 *
 *  Created on: 11.05.2017
 *      Author: RENT
 */

#ifndef NUMID_H_
#define NUMID_H_

class NumID
{
public:

	NumID();
	static int setNextId (int nextId);
	int getId ();
	virtual ~NumID();


private:
	static int nextId;
	int id;
};

#endif /* NUMID_H_ */
