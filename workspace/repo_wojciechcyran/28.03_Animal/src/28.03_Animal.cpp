//============================================================================
// Name        : 03_Animal.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================


#include "Animal.h"
#include "Dog.h"
#include "Cat.h"
#include "Bee.h"
#include <iostream>

using namespace std;

int main()
{
	//Animal anAnimal;

	//Animal someAnimal("t");

	Dog reks("reks");
	Cat mruk("mruk");
	Cat mruuu("mruuu");
	Dog wrumm("wrumm");
	Bee bee ("pszczolka");
	bee.getOwnName();
	//anAnimal.speak();
	//reks.speak();
	//mruk.speak();
	//someAnimal = &reks;
	//someAnimal->speak();
	//someAnimal = &mruk;
	//someAnimal->speak();										// -> this wskaznik na metode z klasy

	cout <<"_----_"<<endl;

	Animal* animalstab[5] = {&bee, &reks, &mruk, &mruuu, &wrumm};		//Animal jest typem dla tablicy (moze byc int, char)
																		//utworzyc tablice w klasie zwierzeta
	std::cout <<reks.giveAName()<<endl;
	   //utworzyc tablice wskaznikow
	std::cout <<mruk.giveAName()<<endl;
	for (int i=0; i<5; i++)							//dziala gdy jest ustawony virtual void w Animal
	{
		animalstab[i]->speak();						//przez indeks
	}
	return 0;
}
