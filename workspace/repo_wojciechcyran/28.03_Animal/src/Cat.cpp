/*
 * Cat.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */
#include <iostream>
#include <string>
#include "Cat.h"

Cat::Cat(std::string givenName):
Animal(givenName)
{
}

void Cat::giveASound()
{
	std::cout << "Miauu !" <<std::endl;
	// Animal::speak();					//okreslajac klase wywolujemy speak z Animal
}
