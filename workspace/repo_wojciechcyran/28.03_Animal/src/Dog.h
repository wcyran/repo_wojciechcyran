

#ifndef DOG_H
#define DOG_H

#include <iostream>
#include "Animal.h"
#include <string>

class Dog: public Animal
{
	public:
	Dog(std::string givenName); // konstruktor pobiera imie;
	void giveASound();
};

#endif
