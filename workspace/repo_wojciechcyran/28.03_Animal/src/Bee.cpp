/*
 * Bee.cpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */
#include <string>
#include "Bee.h"
#include "Animal.h"


Bee::Bee(std::string givenName) :
Animal (givenName)
{
}
void Bee::giveASound()
{
	std::cout <<" Bzzzz" << std::endl;
}
void Bee::getSting()
{
	std::cout << "AaUu" << std::endl;
}
int Bee::getOwnName()
{
    return ownName_Name;
}
