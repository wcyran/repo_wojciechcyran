
#ifndef CAT_H
#define CAT_H

#include <iostream>
#include "Animal.h"
#include <string>


class Cat: public Animal
{
	public:

	Cat(std::string givenName);

	private:
	void giveASound();
};

#endif
