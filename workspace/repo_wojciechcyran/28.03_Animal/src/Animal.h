/*
 * Animal.h
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

// Sygnatura funkcji tylko tutaj deklarujemy virtual przy tworzeniu opisu klasy

#ifndef ANIMAL_H
#define ANIMAL_H

#include <iostream>
#include <string>

class Animal
{
public:                            //konstruktor pobiera
	Animal(std::string givenName);
	void speak();
	std::string giveAName()
	{
		return std::string("my name is: ")+name;
	}

private:
	virtual void giveASound()=0;   // metoda jest wirtualna a klasa jest abstrakcyjna

									// gdy chcemy jakas metode przeciazyc musimy zrobic virtual

	std::string name;							// wysvietli nam anim
};

#endif
