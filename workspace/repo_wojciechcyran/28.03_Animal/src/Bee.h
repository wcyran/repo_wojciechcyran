/*
 * Bee.h
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef BEE_H_
#define BEE_H_

#include "Animal.h"

class Bee: public Animal
{
public:
	Bee(std::string givenName);
	enum ownName {osa,pszczola,trzmiel};


	int getOwnName();
	void setOwnName (ownName other);

    void giveASound();
	void getSting();


private:
	std::string givenName;
	ownName ownName_Name;

};

#endif /* BEE_H_ */
