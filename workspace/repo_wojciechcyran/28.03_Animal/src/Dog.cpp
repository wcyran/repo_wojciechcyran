/*
 * Dog.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#include "Dog.h"
#include <iostream>
#include <string>


Dog::Dog(std::string givenName) :
Animal(givenName)
{
}

void Dog::giveASound()
{
	std::cout << "Hauuu !" <<std::endl;

}
