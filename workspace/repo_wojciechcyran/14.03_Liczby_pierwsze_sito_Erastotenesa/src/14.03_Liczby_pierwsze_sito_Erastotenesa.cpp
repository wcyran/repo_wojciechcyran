//============================================================================
// Name        : 03_Liczby_pierwsze_sito_Erastotenesa.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================


//Program wypisuj�cy liczby pierwsze metod� sita Eratostanesa

#include <iostream>
using namespace std;
const int tab=201;

int main()
{
    int tab[201];
    for(int i = 0; i < 201; ++i)
    {
        tab[i] = 1;
    }

    for(int i = 2; i < 201; ++i)
    {
        if(tab[i] == 1)
        {
            for(int j = 2*i; j < 201; j =j+ i)
            {
                tab[j] = 0;
            }
        }
    }

    for(int i = 2; i < 201; ++i)
    {
        if(tab[i] == 1)
        {
           cout << i << endl;
        }
    }

 return 0;
}
