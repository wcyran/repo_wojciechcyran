/*
 * b.h
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef B_H_
#define B_H_

#include "a.h"
#include "y.h"
#include "z.h"

class b: public a {
public:
	b();
	virtual ~b();
private:
	y y;				//agregacja  klasa b posiada obiekty innych klas
	z z;
};

#endif /* B_H_ */
