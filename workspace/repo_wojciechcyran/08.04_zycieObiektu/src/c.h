/*
 * c.h
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef C_H_
#define C_H_
#include "z.h"
#include "b.h"

class c: public b {
public:
	c();
	virtual ~c();
private:
	z z;
};

#endif /* C_H_ */
