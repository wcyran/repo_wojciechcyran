//============================================================================
// Name        : 04_zycieObiektu.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "a.h"
#include "b.h"
#include "c.h"
using namespace std;


//jesli jest const to nie wychodzi nam referencja ??



void f (a someA)
{
	//cout<<someA.number<<endl;
	//someA.number=5;
	//cout<<someA.number<<endl;
}


int main()
{											//zmienna utworzona na stosie program dziala do klamry
	cout << "!!!Hello World!!!" << endl;
	a a;
	b b;

	std::cout<<"Something more complicated!!!"<<std::endl;
	c c;
	//a.number=2;
	//f(a);
	//cout <<a.number<<endl;
	a *a = new B;
	delete a;

	cout << "!!!Exiting the program!!!" << endl;
	return 0;
}
