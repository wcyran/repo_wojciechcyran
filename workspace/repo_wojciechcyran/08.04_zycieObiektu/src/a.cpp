/*
 * a.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include <iostream>
#include "a.h"

a::a() {
	std::cout <<"Constructing A"<<std::endl;
}

a::~a() {
	std::cout<<"Destroying A"<<std::endl;
}

