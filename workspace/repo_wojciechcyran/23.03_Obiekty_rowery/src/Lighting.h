/*
 * Lighting.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef LIGHTING_H_
#define LIGHTING_H_

class Lighting {
public:

	Lighting front;
	Lighting rear;

	void lightTurnOnAll;
	void lightTurnOfAll;

	Lighting();
	virtual ~Lighting();
};

#endif /* LIGHTING_H_ */
