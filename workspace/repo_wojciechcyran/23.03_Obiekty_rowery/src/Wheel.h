/*
 * Wheel.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef WHEEL_H_
#define WHEEL_H_

class Wheel {
public:
	Wheel();
	Wheel frontWheel;
	Wheel rearWheel;
	virtual ~Wheel();
};

#endif /* WHEEL_H_ */
