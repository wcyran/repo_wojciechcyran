/*
 * Event.h
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef EVENT_H_
#define EVENT_H_
#include <string>

class Event
{
public:
	Event();
	virtual ~Event();
	virtual std::string getEventType() const;

protected:
	static const std::string eventType;
};

#endif /* EVENT_H_ */
