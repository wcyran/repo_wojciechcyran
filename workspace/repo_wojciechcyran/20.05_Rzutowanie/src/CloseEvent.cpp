/*
 * CloseEvent.cpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#include "CloseEvent.h"
#include <string>

const std::string CloseEvent::eventType = "CloseEvent";

CloseEvent::CloseEvent(std::string close): CloseName (close) {
}

CloseEvent::~CloseEvent() {
	// TODO Auto-generated destructor stub
}

//CloseEvent::CloseEvent(std::string close) {
//}

std::string CloseEvent::getCloseType() const {

	return eventType;
}

std::string CloseEvent::getCloseName() const {

	return CloseName;
}
