//============================================================================
// Name        : 05_Rzutowanie.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "OpenFileEvent.h"
#include "Event.h"
#include "CloseEvent.h"
using namespace std;

void displayEvent (Event *event)
{
	std:cout<<"Received: " <<event->getEventType() << " ";
	std::cout<<event->getEventType() <<endl;
	if(event->getEventType()== "OpenFileEvent")
	{
		OpenFileEvent *openEvent = static_cast<OpenFileEvent*>(event);
		std::cout<<"Traying to open file: "<< openEvent->getFileName();
	}
	std::cout<< "\n";
}

int main()
{
	float a =3.5;
	int b = static_cast<int>(a); 	// rzutowanie w czasie kompilacji operator konwersji (int MyFloat::operator(); najczesciej app sterowana zdarzeniami;

	int bb = (int) a;				// usunie nam const - a bedzie nam traktowa� jako liczbe ca�kowit�

	int bbb = int (a);


	cout<<b<<"  "<<bb<<"  "<<bbb<<endl;

	Event gev = Event ();

	OpenFileEvent oev ("a.txt");

	Event *ev= &gev;
	displayEvent(ev);

	std::cout <<ev->getEventType() <<"\n";
	ev = &oev;
	std::cout<< ev->getEventType() <<"\n";
	displayEvent(ev);

	return 0;
}
