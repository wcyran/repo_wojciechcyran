/*
 * CloseEvent.h
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef CLOSEEVENT_H_
#define CLOSEEVENT_H_

#include "Event.h"

class CloseEvent: public Event {
public:
	CloseEvent (std::string close);
	CloseEvent();
	virtual ~CloseEvent();
	std::string getCloseType () const;
	std::string getCloseName () const;
private:
	static const std::string eventType;
	std::string CloseName;
};
#endif /* CLOSEEVENT_H_ */
