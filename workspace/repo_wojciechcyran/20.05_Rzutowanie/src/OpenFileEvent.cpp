/*
 * OpenFileEvent.cpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#include "OpenFileEvent.h"
#include <string>

const std::string OpenFileEvent::eventType = "OpenFileEvent";

OpenFileEvent::OpenFileEvent() {
	// TODO Auto-generated constructor stub

}

OpenFileEvent::OpenFileEvent(std::string fileToOpen):fileName (fileToOpen)
{
}

OpenFileEvent::~OpenFileEvent() {
	// TODO Auto-generated destructor stub
}

std::string OpenFileEvent::getEventType() const
{
	return eventType;
}

std::string OpenFileEvent::getFileName() const
{
	return fileName;
}
