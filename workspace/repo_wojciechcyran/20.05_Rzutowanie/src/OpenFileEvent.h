/*
 * OpenFileEvent.h
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef OPENFILEEVENT_H_
#define OPENFILEEVENT_H_

#include "Event.h"

class OpenFileEvent: public Event
{
public:
	OpenFileEvent (std::string fileToOpen);
	OpenFileEvent();
	virtual ~OpenFileEvent();
	std::string getEventType () const;
	std::string getFileName () const;
private:
	static const std::string eventType;
	std::string fileName;
};

#endif /* OPENFILEEVENT_H_ */
