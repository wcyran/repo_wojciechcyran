/*
 * ClassA.h
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#ifndef CLASSA_H_
#define CLASSA_H_
#include <iostream>
#include <string>

struct product
{
	enum BikeType ;
	enum BikeModel;
	int limit;
};

const int tSize=4;
class ClassA {
public:

	Bike();
	virtual ~Bike();


private:

product productTab[tSize];
int choice;
int limitP;
int *wskLimit;
float money;
float mTemp;
float *wskPrice;
std::string *wskProductName;

};


#endif /* CLASSA_H_ */
