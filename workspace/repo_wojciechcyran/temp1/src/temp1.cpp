//============================================================================
// Name        : temp1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
const int tab=101;

int main()
{
    int tab[101];
    for(int i = 0; i < 101; ++i)
    {
        tab[i] = 1;
    }

    for(int i = 2; i < 101; ++i)
    {
        if(tab[i] == 1)
        {
            for(int j = 2*i; j < 101; j =j+ i)
            {
                tab[j] = 0;
            }
        }
    }

    for(int i = 2; i < 101; ++i)
    {
        if(tab[i] == 1) {
           cout << i << " ";
        }
    }

    cout <<endl;
    return 0;
}
