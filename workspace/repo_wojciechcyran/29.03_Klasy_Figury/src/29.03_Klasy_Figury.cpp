//============================================================================
// Name        : 03_Klasy_Figury.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Ksztalt.h"
#include "kwadrat.h"
#include "trojkat.h"
#include "kolo.h"
#include <string>


int main()
{
	//Ksztalt k ("Ksztalt");
	kwadrat kw ("Kwadrat");
	kolo ko ("Kolo,2");
	trojkat t ("Trojkat,3,2");

	//k.speak();
	kw.speak();
	ko.speak();
	t.speak();

	std::cout << std::endl;

	 std::cout <<"_------------------- Opis------------------_"<<std::endl;

	kw.description();
	ko.description();
	t.description();

	std::cout<< std::endl;

	std::cout <<"_-- Po dziedziczeniu z klasy bazowej kszalt ---_"<<std::endl;

	std::cout << "Opis: " << kw.descriptionAll () << std::endl;
	std::cout << "Opis: " << ko.descriptionAll ()<< std::endl;



	std::cout<< std::endl;

	std::cout <<"_------ Rysowanie ----_"<<std::endl;

	kw.rys_kwadrat();
	std::cout<<std::endl;
	t.rys_troj();

	std::cout<< std::endl<<"_------ Pole powierzchni ----_"<<std::endl;


	kw.surfaceArea();
	ko.surfaceArea();
	t.surfaceArea();

	std::cout << "Pole = "<< kw.surfaceArea ()<< std::endl;
	std::cout << "Pole = "<< ko.surfaceArea () << std::endl;
	std::cout << "Pole = "<< t.surfaceArea () << std::endl;


	std::cout <<"_-- Po dziedziczeniu z klasy bazowej kszalt ---_"<<std::endl;


	//std::cout << k.giveAName()<<std::endl;
	std::cout << kw.giveAName()<<std::endl;
	std::cout << ko.giveAName()<<std::endl;
	std::cout << t.giveAName()<<std::endl;

	std::cout <<"_-------Tabela--------_"<<std::endl;

	 const int num=3;
	 Ksztalt* tab[3]= {&kw, &ko, &t};

	 for (int i=0; i<num; i++)
	 {
		 std::cout << tab[i] -> surfaceArea() << std::endl; // wyświetlanie pola
	 }


	 //Ksztalt k ("Ksztalt");

	/*std::cout << "Opis: " << kw.description() <<std::endl;
	std::cout << "Opis: " << ko.description () <<std::endl;
	std::cout << "Opis: " << t.description () <<std::endl;
	*/

	return 0;
}
