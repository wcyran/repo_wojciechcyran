/*
 * trojkat.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef TROJKAT_H_
#define TROJKAT_H_
#include <string>
#include "Ksztalt.h"

class trojkat: public Ksztalt
{
public:
	trojkat (std::string giveName);
	void speak ();
	void description ();
	double surfaceArea ();
	void rys_troj ();


private:
	double a=2;		//poziom
	double h=5;		//pion
	double pole;
};

#endif /* TROJKAT_H_ */
