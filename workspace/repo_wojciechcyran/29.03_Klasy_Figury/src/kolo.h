/*
 * kolo.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef KOLO_H_
#define KOLO_H_

#include <iostream>
#include <string>
#include "Ksztalt.h"

class kolo: public Ksztalt
{
public:

	const double pi=3.14;

	kolo (std::string giveName);

	double surfaceArea();
	void speak ();
	void description ();

private:
	double p=2;
	double pole;

};

#endif /* KOLO_H_ */
