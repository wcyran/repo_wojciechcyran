/*
 * Ksztalt.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef KSZTALT_H_
#define KSZTALT_H_
#include <string>


class Ksztalt
{
public:

	Ksztalt(std:: string giveName);

	void speak ();
	std::string giveAName ()
	{
		return std::string("Ksztalt");
	}
	virtual double surfaceArea ()=0;
	virtual void rys_kwadrat ();
	virtual void rys_troj ();
	void description();
	std::string descriptionAll ()
	{
		return std::string ("Jestem z rodziny figur");
	}

	std::string name;

private:

};

#endif /* KSZTALT_H_ */
