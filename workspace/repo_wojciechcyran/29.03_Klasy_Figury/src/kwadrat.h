/*
 * kwadrat.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef KWADRAT_H_
#define KWADRAT_H_

#include <iostream>
#include "Ksztalt.h"
#include <string>

class kwadrat: public Ksztalt
{
public:
	kwadrat (std::string giveName);
    double surfaceArea();
	void speak();
	void rys_kwadrat ();
	void description ();

private:
	double pole;
	double a=5;
	int i,j;
};

#endif /* KWADRAT_H_ */
