//============================================================================
// Name        : 05_Wyjatki_cz3.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <math.h>
#include <exception>

using namespace std;
//spoza dziedziny -> domain error

class edomException : public exception {
	virtual const char* what() const throw (){
		return "EDOM exception";
	}

};
class erangeException : public exception {
	virtual const char* what() const throw (){
		return "ERANGE exception";
	}

};

float myPow (float x, float y)
{

	float wynik  = pow(x,y);
	if(errno == EDOM){
		edomException e;
		throw e;
	}
	if (errno == ERANGE){
		erangeException e;
		throw e;
	}
	return wynik;

}
//myLog (int x)

int main()
{
	int x,y;
	cout<<"Podaj podstawe potegi: "<<endl;
	cin >>x;
	cout<<"Podaj wykladnik potegi: "<<endl;
	cin >>y;
	try{
	int z = myPow(x, y);

	//int b = log (-10);

	}
	catch(exception & e){									//deufult typ
				cout<<"exception "<<e.what();
	}
		return 0;
}
